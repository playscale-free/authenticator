# Example

This is sample code that will not build for a device platform as it
requires specific application integration with Facebook and Firebase.

It needs API keys and google-services.json with a registered
application.

The sample code does build with the provided widget_test.dart and
will execute with `flutter test`.

To see it working on a device, the project needs to be modified with
valid credentials from Google and Facebook.

## Latest

Browse the latest on the [authenticator project at gitlab](https://gitlab.com/playscale-free/authenticator/tree/master/example).
