import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:authenticator/authenticator.dart';

void main() {
  var auth = Authenticator();
  runApp(MyApp(auth));
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application
  final Authenticator auth;
  MyApp(Authenticator authenticator) : auth = authenticator;

  @override
  Widget build(BuildContext context) {
    return ListenableProvider<Authenticator>.value(
        value: auth,
        child: MaterialApp(
          title: 'Authenticator Demo',
          theme: ThemeData(
            // This is the theme of your application.
            //
            // Try running your application with "flutter run". You'll see the
            // application has a blue toolbar. Then, without quitting the app, try
            // changing the primarySwatch below to Colors.green and then invoke
            // "hot reload" (press "r" in the console where you ran "flutter run",
            // or simply save your changes to "hot reload" in a Flutter IDE).
            // Notice that the counter didn't reset back to zero; the application
            // is not restarted.
            primarySwatch: Colors.blue,
          ),
          home: Root(),
        ));
  }
}

class Root extends StatelessWidget {
  Root() : super(key: ValueKey('Root Page'));

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Authenticator>(context);
    return ListenableProvider<Authenticator>.value(
        value: auth,
        child: Consumer(builder: (context, Authenticator auth, _) {
          switch (auth.authState) {
            case AuthState.AUTHENTICATED:
              return Home();
              break;
            default:
              return Login();
              break;
          }
        }));
  }
}

class Login extends StatelessWidget {
  Login() : super(key: ValueKey('Login Page'));

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Authenticator>(context);
    return ListenableProvider<Authenticator>.value(
        value: auth,
        child: Consumer(builder: (context, Authenticator auth, _) {
          return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Text("Sign in with ..."),
                FlatButton(
                    key: ValueKey('signInWithGoogle button'),
                    child: Text('Google'),
                    onPressed: () {
                      auth.signInWithGoogle();
                    }),
                FlatButton(
                    key: ValueKey('signInWithFacebook button'),
                    child: Text("Facebook"),
                    onPressed: () {
                      auth.signInWithFacebook();
                    })
              ]);
        }));
  }
}

class Home extends StatelessWidget {
  Home() : super(key: ValueKey('Home Page'));

  @override
  Widget build(BuildContext context) {
    final auth = Provider.of<Authenticator>(context);
    return ListenableProvider<Authenticator>.value(
        value: auth,
        child: Consumer(builder: (context, Authenticator auth, _) {
          return FlatButton(
              key: ValueKey('signOut button'),
              child: Text("Sign out"),
              onPressed: () {
                auth.signOut();
              });
        }));
  }
}
