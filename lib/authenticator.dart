// Copyright (c) 2019 Playscale PTE LTD - All Rights Reserved
// See LICENSE, included with this distribution, for more details
// email: contact@playscale.io

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:authenticator/mock_facebook_login_channel.dart';
import 'package:authenticator/mock_firebase_auth_channel.dart';
import 'package:authenticator/mock_google_sign_in_channel.dart';

/// AuthenticationState has a few possibilities:
/// - the app has started, and the first call to FirebaseAuth
///   has been sent and the app is waiting for a response
/// - Firebase auth has returned NULL for the user, so the user
///   needs to authenticate
/// - The user has submitted their credentials to authenticate
///   and the app is waiting for a response
/// - The user is authenticated (either during app startup, or as a
///   result of a login attempt)
enum AuthState { UNINITIALIZED, UNAUTHENTICATED, AUTHENTICATING, AUTHENTICATED }

/// Currently support authentication methods.
enum AuthType { UNKNOWN, EMAIL, GOOGLE, FACEBOOK }

/// Authenticator is a ChangeNotifier that wraps various authentication
/// methods supported through Firebase. Google, Facebook and email
/// authentication are currently supported.
///
/// It also supports mocked platform channels so authentication flow
/// can be included in flutter tests.
///
/// Usage:
/// ```dart
/// void main() {
///   auth = Authenticator();
///   await auth.signInWithFacebook();
///   await auth.signOut();
/// }
/// ```
///
/// Since it is a ChangeNotifier, it can notify widgets when there
/// is a change. For example:
/// ```dart
/// class LoginPage extends StatelessWidget {
///   static final loginPageKey = ValueKey(loginPageKeyName);
///
///   LoginPage() : super(key: loginPageKey);
///
///   @override
///   Widget build(BuildContext context) {
///     final auth = Provider.of<Authenticator>(context);
///
///     return ListenableProvider<Authenticator>.value(
///       value: locator<Authenticator>(),
///       child: Scaffold(
///           appBar: AppBar(title: Text('Welcome!'), centerTitle: true),
///           body: Container(
///             child: Center(
///               child: Column(
///                 mainAxisSize: MainAxisSize.max,
///                 mainAxisAlignment: MainAxisAlignment.center,
///                 children: <Widget>[
///                   LoginLogoView(),
///                   auth.authState != AuthState.AUTHENTICATED
///                       ? SignInButtonsView()
///                       : Container()
///                 ],
///               ),
///             ),
///           )),
///     );
///   }
/// }
/// ```
/// And in a SignInButtonsView, there may be buttons to login with
/// Google, Facebook, email, or create a new account using email.
/// One of the onPressed values for a button could execute this code
/// to sign in with google.
///
/// ```dart
///   void _signInWithGoogleButton() {
///     var auth = Provider.of<Authenticator>(context);
///     setState(() {
///       signingIn = true;
///     });
///     auth.signInWithGoogle();
/// }
///
/// ```
///
/// Mixing Authenticator with packages like GetIt to turn it into
/// a globally accessible object can be convenient. There should only
/// be one Authenticator at any point in time anyway.
class Authenticator with ChangeNotifier {
  final _googleSignIn = GoogleSignIn();
  final _firebaseAuth = FirebaseAuth.instance;
  final _facebookLogin = new FacebookLogin();

  FirebaseUser _user;

  AuthType _authType = AuthType.UNKNOWN;
  AuthState _authState = AuthState.UNINITIALIZED;
  String _currentSignInProvider = '';
  String get currentSignInProvider => _currentSignInProvider;
  FirebaseUser get user => _user;

  // mock overrides when the Authenticator is in mock mode
  String _mockEmail;
  String get mockEmail => _mockEmail;
  String _mockPassword;
  String get mockPassword => _mockPassword;
  bool _mocked =
      false; // needed to manually trigger _onAuthStateChanged when mocking

  AuthState get authState => _authState;

  /// The default constructor should be used in standard flutter
  /// applications. If you are using something like GetIt, you can
  /// follow the `setup` pattern using Authenticator like so:
  ///
  /// ```dart
  /// import 'package:get_it/get_it.dart';
  /// import 'package:authenticator/authenticator.dart';
  ///
  /// GetIt locator = GetIt.instance;
  ///
  /// void setupLocator() {
  ///   locator.registerSingleton(Authenticator());
  /// }
  /// ```
  ///
  /// Then later access the Authenticator singleton in code:
  /// ```dart
  /// var auth = locator<Authenticator>();
  /// ```
  Authenticator() {
    _firebaseAuth.onAuthStateChanged.listen(_onAuthStateChanged);
  }

  /// The named constructor best used with unit tests
  /// (e.g. `flutter test`).
  ///
  /// This will create platform channel mocks for Facebook, Google
  /// and Firebase (email, for example). See the unit tests for
  /// this package as an example of using a mocked Authenticator.
  ///
  /// ```dart
  /// void main() {
  ///   locator.allowReassignment = true;
  ///   testWidgets('Mock email sign in from splash to home page',
  ///       (WidgetTester tester) async {
  ///     locator.registerSingleton<Authenticator>(Authenticator.mocked());
  ///     var auth = locator<Authenticator>();
  ///     expect(auth.authState, isNot(AuthState.AUTHENTICATED),
  ///         reason: 'No user should be authenticated at this point');
  ///     // Build our app and trigger a frame.
  ///     await tester.pumpWidget(Root());
  ///
  ///     expect(find.byKey(LoginPage.loginPageKey), findsOneWidget);
  ///
  ///     locator<Authenticator>().signInWithGoogle();
  ///     // verify that changing auth state switches to the home page
  ///     await tester.pumpAndSettle(Duration(milliseconds: 1000));
  ///     expect(find.byKey(HomePage.homePageKey), findsOneWidget);
  ///     locator<Authenticator>().signOut();
  ///     locator.reset();
  ///   });
  /// }
  /// ```
  static Authenticator createMocked(
      {String email = 'testemailaddress@testmail.com',
      String password = '123456'}) {
    MockFacebookLoginChannel();
    MockFirebaseAuthChannel();
    MockGoogleSignInChannel();
    var auth = Authenticator();
    auth._mocked = true;
    auth._mockEmail = email;
    auth._mockPassword = password;
    return auth;
  }

  String _getUserIdFromAuthResult(AuthResult authResult) {
    if (authResult == null) return null;

    FirebaseUser user = authResult.user;

    if (user.email == null) return null;
    if (user.displayName == null) return null;
    if (user.isAnonymous) return null;

    return user.uid;
  }

  /// Just like it says on the tin. This will initiate a Facebook
  /// sign in. On devices, this may, at least once, open a system
  /// overlay asking the user if they would like to sign in with
  /// Facebook.
  ///
  /// As soon as the call is made, the Authenticator's state will
  /// be set to `AuthState.AUTHENTICATING`, a notification
  /// will be sent anyone listening to the provider so they can
  /// redraw with the new state, and an async process of waiting
  /// for the user to finish signing in will begin.
  ///
  /// Once the user has either chosen to login with Facebook, or
  /// cancel, a notification will be triggered again with a new
  /// AuthState for Authenticator. It will either be
  /// `AuthState.AUTHENTICATED` or `AuthState.UNAUTHENTICATED`
  ///
  /// If authentication was successful, the AuthType will be set to
  /// AuthType.FACEBOOK.
  Future<String> signInWithFacebook() async {
    _currentSignInProvider = 'facebook.com';
    _authState = AuthState.AUTHENTICATING;
    notifyListeners();
    var result = await _facebookLogin.logIn(['email', 'public_profile']);
    if (result.status == FacebookLoginStatus.loggedIn) {
      final AuthCredential credential = FacebookAuthProvider.getCredential(
          accessToken: result.accessToken.token);
      final AuthResult authResult =
          await _firebaseAuth.signInWithCredential(credential);
      if (await authResult.user.getIdToken() == null) return null;
      var uid = _getUserIdFromAuthResult(authResult);
      if (uid != null) {
        _authType = AuthType.FACEBOOK;
        _authState = AuthState.AUTHENTICATED;
      }
      if (_mocked) {
        _onAuthStateChanged(authResult.user);
      }
      return uid;
    }
    if (_mocked) {
      _onAuthStateChanged(null);
    }

    return null;
  }

  /// This will initiate a Google
  /// sign in. On devices, this may, at least once, open a system
  /// overlay asking the user if they would like to sign in with
  /// Google, or select which account of they use more than one.
  ///
  /// As soon as the call is made, the Authenticator's state will
  /// be set to `AuthState.AUTHENTICATING`, a notification
  /// will be sent anyone listening to the provider so they can
  /// redraw with the new state, and an async process of waiting
  /// for the user to finish signing in will begin.
  ///
  /// Once the user has either chosen to login with Google, or
  /// cancel, a notification will be triggered again with a new
  /// AuthState for Authenticator. It will either be
  /// `AuthState.AUTHENTICATED` or `AuthState.UNAUTHENTICATED`
  /// If authentication was successful, the AuthType will be set to
  /// AuthType.GOOGLE.
  Future<String> signInWithGoogle() async {
    _authState = AuthState.AUTHENTICATING;
    notifyListeners();

    final GoogleSignInAccount googleSignInAccount =
        await _googleSignIn.signIn();

    if (googleSignInAccount == null) {
      _authState = AuthState.UNAUTHENTICATED;
      if (_mocked) {
        _onAuthStateChanged(null);
      }
      return null;
    }
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult =
        await _firebaseAuth.signInWithCredential(credential);
    var uid = _getUserIdFromAuthResult(authResult);
    if (uid != null) {
      _authType = AuthType.GOOGLE;
      _authState = AuthState.AUTHENTICATED;
      if (_mocked) {
        _onAuthStateChanged(authResult.user);
      }
    }

    return uid;
  }

  /// This will initiate an email+password login through Firebase.
  /// The user must have an existing account in Firebase for this
  /// to succeed. (See `createUserWithEmailAndPassword()`)
  ///
  /// As soon as the call is made, the Authenticator's state will
  /// be set to `AuthState.AUTHENTICATING`, a notification
  /// will be sent anyone listening to the provider so they can
  /// redraw with the new state, and an async process of waiting
  /// for the user to finish signing in will begin.
  ///
  /// Once the operation has completed
  /// a notification will be triggered again with a new
  /// AuthState for Authenticator. It will either be
  /// `AuthState.AUTHENTICATED` or `AuthState.UNAUTHENTICATED`
  /// If authentication was successful, the AuthType will be set to
  /// AuthType.EMAIL.
  ///
  /// If the user does not exist, or the password is incorrect, this
  /// will throw a PlatformException with a code reflecting which of
  /// those cases happened.
  Future<String> signInWithEmailAndPassword(
      String email, String password) async {
    _authState = AuthState.AUTHENTICATING;
    notifyListeners();
    AuthResult authResult;
    try {
      authResult = await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      if (authResult != null &&
          authResult.user != null &&
          authResult.user.uid != null) {
        _authState = AuthState.AUTHENTICATED;
        _authType = AuthType.EMAIL;
      }
    } catch (e) {
      _authState = AuthState.UNAUTHENTICATED;
      if (_mocked) {
        _onAuthStateChanged(null);
      }
      throw (e);
    }
    if (_mocked) {
      _onAuthStateChanged(authResult.user);
    }

    return authResult.user.uid;
  }

  /// Starts an asynchronous call to Firebase to create a new account
  /// for the email address and password supplied. It will immediately
  /// set Authenticator's auth state to `AuthState.AUTHENTICATING`
  /// and notify any listeners for changes so the UI can be updated
  /// appropriately.
  ///
  /// If the user already exists, a `PlatformException` will throw
  /// and can be caught to advise the user to do something like reset
  /// their password (see `sendPasswordResetEmail`) for more
  /// information.
  Future<String> createUserWithEmailAndPassword(
      String email, String password) async {
    _authState = AuthState.AUTHENTICATING;
    notifyListeners();
    try {
      AuthResult authResult = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);

      if (authResult != null &&
          authResult.user != null &&
          authResult.user.uid != null) {
        _authState = AuthState.AUTHENTICATED;
      }

      if (_mocked) {
        _onAuthStateChanged(authResult.user);
      }

      return authResult.user.uid;
    } catch (e) {
      _authState = AuthState.UNAUTHENTICATED;
      if (_mocked) {
        _onAuthStateChanged(null);
      }
      notifyListeners();
      throw (e);
    }
  }

  /// Returns the current user, or null if no user has been
  /// authenticated.
  Future<String> currentUser() async {
    FirebaseUser user = await _firebaseAuth.currentUser();
    if (user == null) {
      return null;
    }

    return user.uid;
  }

  /// Signs out the user. Once the operation has completed
  /// it will set the current user to null,
  /// set the authentication state for the Authenticator to
  /// `AuthState.UNAUTHENTICATED` and set the authentication
  /// type to `AuthType.UNKNOWN`. A notification will then be
  /// sent to all listeners.
  Future<void> signOut() async {
    switch (_authType) {
      case AuthType.GOOGLE:
        await _googleSignIn.signOut();
        break;
      case AuthType.FACEBOOK:
        await _facebookLogin.logOut();
        break;
      case AuthType.EMAIL:
        await _firebaseAuth.signOut();
        break;
      case AuthType.UNKNOWN:
        break;
    }
    await _firebaseAuth.signOut();
    _authState = AuthState.UNAUTHENTICATED;
    _authType = AuthType.UNKNOWN;
    if (_mocked) {
      _onAuthStateChanged(null);
    }
    notifyListeners();
  }

  /// Initiates a password reset flow. Firebase will send an email to
  /// the user with links to reset their password. This is an async
  /// method that doesn't return anything or send any notifications.
  Future<void> sendPasswordResetEmail(String emailAddress) async {
    await _firebaseAuth.sendPasswordResetEmail(email: emailAddress);
  }

  void _onAuthStateChanged(FirebaseUser user) {
    _user = user;
    if (user == null) {
      _authState = AuthState.UNAUTHENTICATED;
    } else {
      _authState = AuthState.AUTHENTICATED;
    }
    notifyListeners();
  }
}
