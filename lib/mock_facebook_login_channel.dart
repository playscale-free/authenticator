// Copyright (c) 2019 Playscale PTE LTD - All Rights Reserved
// See LICENSE, included with this distribution, for more details
// email: contact@playscale.io

import 'package:flutter/services.dart';
import 'dart:convert';

/// Replaces platform channel responses to requests from
/// flutter_facebook_login.dart in the flutter_facebook_login package.
///
/// Instantiate this by calling
/// ```dart
/// MockFacebookLoginChannel();
/// ```
/// There are static members that can be set prior to
/// authentication that will affect the behavior of this mock.
/// You can override the responses by providing your own json
/// data (logIn and getCurrentToken), as well as enabling
/// extra debugging and assertions by setting
/// ```dart
/// throwOnUnhandledChannelMessages = true|false
/// ```
/// See also static String members
/// ```dart
/// MockFacebookLoginChannel.loginResponse
/// ```
/// and
/// ```dart
/// MockFacebookLoginChannel.getCurrentAccessTokenResponse
/// ```
/// documentation for more information. In VSCode, just hover
/// over any declaration of either to get more information.
class MockFacebookLoginChannel {
  /// When set to
  /// `true`
  /// it will throw when channel calls are not handled.
  /// If this happens, it means that a call that was made to the
  /// mocked platform channel was unhandled and needs to be implemented
  /// in. Defaults to ```false```.
  static bool throwOnUnhandledChannelMessages = false;

  /// When set to an exception, this will throw the specified
  /// exception. This is useful for testing
  /// boilerplate try/catch code in tests for coverage and correctness.
  ///
  /// Defaults to `null`
  static Exception throwOnEveryChannelMessage;

  /// change this to override the response to a "logIn" request from facebook
  /// The default is (from json)
  ///
  /// ```json
  /// {
  ///   "accessToken": {
  ///
  ///       "expires": 0, // CALCULATED if 0, will us ((DateTime.now().millisecondsSinceEpoch / 1000) + (60 * 60 * 24)).round()
  ///       "declinedPermissions": [],
  ///       "permissions": [], // CALCULATED if empty, will populate from the request as valid
  ///       "userId": "42", // note, this number must be a string!
  ///       "token": "test_token"
  ///   },
  ///   "status": "loggedIn"
  /// }
  /// ```
  ///
  /// For example, to force an error status in tests:
  /// ```dart
  /// MockFacebookLoginChannel.loginRepsonse['status'] = 'error';
  /// ```
  static Map loginResponse = json.decode(_loginResponse);

  /// This will reset the loginResponse to default values.
  static void resetLoginResponse() {
    if (loginResponse != null) {
      loginResponse.clear();
    }
    loginResponse = json.decode(_loginResponse);
  }

  static const String _loginResponse = '''
  {
    "accessToken": {
        "expires": 0,
        "declinedPermissions": [],
        "permissions": [],
        "userId": "42",
        "token": "test_token"
    },
    "status": "loggedIn"
}
  ''';

  /// NOTE: NOT YET IMPLEMENTED
  /// The facebook auth plugin calls this method, but there is no test
  /// to trigger it yet. If you have a test case that can trigger this,
  /// please share it with a merge request and/or an issue.
  ///
  /// change this to override the response to a "logIn" request from facebook
  /// The default is
  ///
  /// ```json
  /// {
  ///
  ///       "expires": 0, // CALCULATED if 0, will us ((DateTime.now().millisecondsSinceEpoch / 1000) + (60 * 60 * 24)).round()
  ///       "declinedPermissions": [],
  ///       "permissions": [], // CALCULATED if empty, will populate from the request as valid
  ///       "userId": "42", // note, this number must be a string!
  ///       "token": "test_token"
  /// }
  /// ```
  ///
  /// Change the default by setting
  /// ```dart
  /// MockFacebookLoginChannel.getCurrentAccessTokenResponse = '<your json object>'
  /// ```
  static Map currentAccessTokenResponse =
      json.decode(_currentAccessTokenResponse);

  /// This will set the currentAccessTokenResponse map to default values.
  static void resetCurrentAccessTokenResponse() {
    if (currentAccessTokenResponse != null) {
      currentAccessTokenResponse.clear();
    }
    currentAccessTokenResponse = json.decode(_currentAccessTokenResponse);
  }

  static String _currentAccessTokenResponse = '''
  {
        "expires": 0,
        "declinedPermissions": [],
        "permissions": [],
        "userId": "42",
        "token": "test_token"
  }
  ''';

  static const String platformChannelName =
      'com.roughike/flutter_facebook_login';
  static final _facebookLoginChannel = MethodChannel(platformChannelName);

  MockFacebookLoginChannel() {
    _facebookLoginChannel.setMockMethodCallHandler(_methodCall);
  }

  Future<dynamic> _methodCall(MethodCall call) async {
    if (throwOnEveryChannelMessage != null) {
      throw (throwOnEveryChannelMessage);
    }
    switch (call.method) {
      case 'logIn':
        if (loginResponse.containsKey('accessToken')) {
          if (loginResponse['accessToken'].containsKey('expires') &&
              loginResponse['accessToken']['expires'] == 0) {
            // calculate the expiration
            loginResponse['accessToken']['expires'] =
                ((DateTime.now().millisecondsSinceEpoch / 1000) +
                        (60 * 60 * 24))
                    .round();
          }

          if (loginResponse['accessToken'].containsKey('permissions')) {
            List<dynamic> permissions =
                loginResponse['accessToken']['permissions'];
            if (permissions.length == 0) {
              // grab the permissions requested
              for (var perm in call.arguments['permissions'] as List<dynamic>) {
                loginResponse['accessToken']['permissions'].add(perm);
              }
            }
          }
        }

        return loginResponse;
        break;
      // Not implemented and not testable through the facebook auth plugin
      // Once https://github.com/flutter/flutter/issues/42660 is resolved
      // and there is some way to trigger this from a test, uncomment
      // this code to ensure it behaves as expected
      //
      // case 'getCurrentAccessToken':
      //   Map result = json.decode(loginResponse);
      //   if (result.containsKey('expires') && result['expires'] == 0) {
      //     // calculate the expiration
      //     result['expires'] =
      //         ((DateTime.now().millisecondsSinceEpoch / 1000) + (60 * 60 * 24))
      //             .round();
      //   }

      //   if (result.containsKey('permissions')) {
      //     List<dynamic> permissions = result['permissions'];
      //     if (permissions.length == 0) {
      //       // grab the permissions requested
      //       for (var perm in call.arguments['permissions'] as List<dynamic>) {
      //         result['permissions'].add(perm);
      //       }
      //     }
      //   }

      //   return result;
      //   break;
      //
      // Not implemented and not testable through the facebook auth plugin
      // via Firebase. Explicity calling logOut() on the FacebookLogin plugin
      // does not drigger this.
      //
      // case 'logOut':
      //   return null;
      //   break;
      default:
        if (throwOnUnhandledChannelMessages) {
          throw (PlatformException(
              code: 'UnhandledMockChannelCall', message: "$call"));
        }
        break;
    }
    return null;
  }
}
