# Changelog

## [0.1.3] -

- Link to the repository for the example

## [0.1.2] - 2019-10-21

- Some pedantic documentation corrections to avoid confision when
  using the package.

## [0.1.1] - 2019-10-21

- Can provide custom exceptions to force platform channel mocks to
  throw.

- Tests can trigger platform channel mocks to always throw on any
  call to the platform. Useful for forcing failure cases in application
  code during unit tests.

- Example tests and application added to the package and repository.

- Generate dartdoc documentation for the package.

## [0.0.1] - 2019-10-15

- Initial public release of Playscale's Authenticator package.
