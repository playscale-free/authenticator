import 'package:authenticator/mock_facebook_login_channel.dart';
import 'package:authenticator/mock_firebase_auth_channel.dart';
import 'package:authenticator/mock_google_sign_in_channel.dart';
import 'package:flutter/services.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:authenticator/authenticator.dart';

void main() {
  var auth = Authenticator.createMocked();

  test('Mock override resets work', () async {
    MockFacebookLoginChannel.resetLoginResponse();
    MockFacebookLoginChannel.loginResponse = null;
    MockFacebookLoginChannel.resetLoginResponse();

    MockFacebookLoginChannel.resetCurrentAccessTokenResponse();
    MockFacebookLoginChannel.currentAccessTokenResponse = null;
    MockFacebookLoginChannel.resetCurrentAccessTokenResponse();

    MockGoogleSignInChannel.resetGoogleSignInResponse();
    MockGoogleSignInChannel.googleSignInResponse = null;
    MockGoogleSignInChannel.resetGoogleSignInResponse();

    MockFirebaseAuthChannel.resetIdTokenResponse();
    MockFirebaseAuthChannel.idTokenResponse = null;
    MockFirebaseAuthChannel.resetIdTokenResponse();

    MockFirebaseAuthChannel.resetAuthResultResponse();
    MockFirebaseAuthChannel.authResultResponse = null;
    MockFirebaseAuthChannel.resetAuthResultResponse();
  });
  test('Facebook authentication', () async {
    var uid = await auth.signInWithFacebook();
    expect(uid, 'test_uid',
        reason: 'signInWithFacebook did not return "test_uid" as the result');
    expect(uid, auth.user.uid,
        reason: 'uid returned does not match the uid in auth');
    expect(auth.currentSignInProvider, 'facebook.com',
        reason: 'Facebook sign in did not set the provider to facebook.com');
    await auth.signOut();
    expect(auth.authState, AuthState.UNAUTHENTICATED,
        reason: 'User is not in an UNAUTHENTICATED state after signing out!');
  });

  test('Facebook authentication fail no resut', () async {
    MockFacebookLoginChannel.loginResponse['status'] = 'error';
    var uid = await auth.signInWithFacebook();
    expect(uid, isNull,
        reason:
            'signInWithFacebook should not return a result when there is an error');
    expect(auth.authState, AuthState.UNAUTHENTICATED,
        reason: 'User is not in an UNAUTHENTICATED state after signing out!');
    await auth.signOut();
    MockFacebookLoginChannel.resetLoginResponse();
  });

  test('Google authentication', () async {
    var uid = await auth.signInWithGoogle();
    expect(auth.authState, AuthState.AUTHENTICATED);
    expect(uid, 'test_uid',
        reason: 'signInWithGoogle did not return "test_uid" as the result');
    await auth.signOut();
    expect(auth.authState, AuthState.UNAUTHENTICATED,
        reason: 'User is not in an UNAUTHENTICATED state after signing out!');
  });

  test('Google authentication fails', () async {
    MockGoogleSignInChannel.resetGoogleSignInResponse();
    MockGoogleSignInChannel.googleSignInResponse = null;
    await auth.signInWithGoogle();
    expect(auth.authState, AuthState.UNAUTHENTICATED);
    await auth.signOut();
    MockGoogleSignInChannel.resetGoogleSignInResponse();
  });

  test('Get uid from firebase user after authenticating', () async {
    await auth.signInWithGoogle();
    var uid = await auth.currentUser();
    expect(uid, 'test_uid',
        reason:
            'currentUser() did not return "test_uid" as the result after signing in');
  });

  test('Email address and password authentication', () async {
    var uid = await auth.signInWithEmailAndPassword(
        'testemailaddress@testmail.com', '123456');
    expect(uid, 'test_uid',
        reason:
            'signInWithEmailAndPassword did not return "test_uid" as the result');
    await auth.signOut();
    expect(auth.authState, AuthState.UNAUTHENTICATED,
        reason: 'User is not in an UNAUTHENTICATED state after signing out!');
  });

  test('Bad email address', () async {
    String uid;
    try {
      await auth.signInWithEmailAndPassword(
          'badaddress@baddomain.com', '123456');
    } catch (e) {
      PlatformException pe = e as PlatformException;
      expect(pe, isNotNull,
          reason:
              'An exception was thrown, but it was not a PlatformException');
      expect(pe.code, 'ERROR_USER_NOT_FOUND',
          reason:
              'An exception was thrown, but it was not the expected PlatformException code: ERROR_USER_NOT_FOUND');
    }
    expect(uid, isNull,
        reason: 'signInWithEmailAndPassword did not return null as the result');
    expect(auth.authState, AuthState.UNAUTHENTICATED,
        reason: 'User is not in an UNAUTHENTICATED state after signing out!');
  });

  test('Bad password', () async {
    String uid;
    expect(auth.mockEmail, 'testemailaddress@testmail.com',
        reason:
            'The test requires the mocked email address to be testemailaddress@testmail.com, but it is ${auth.mockEmail} instead');
    expect(auth.mockPassword, isNot('234567'),
        reason:
            'The test requires that the mocked password is not 234567, but it is');
    try {
      await auth.signInWithEmailAndPassword(
          'testemailaddress@testmail.com', '234567');
    } catch (e) {
      PlatformException pe = e as PlatformException;
      expect(pe, isNotNull,
          reason:
              'An exception was thrown, but it was not a PlatformException');
      expect(pe.code, 'ERROR_WRONG_PASSWORD',
          reason:
              'An exception was thrown, but it was not the expected PlatformException code: ERROR_WRONG_PASSWORD');
    }
    expect(uid, isNull,
        reason: 'signInWithEmailAndPassword did not return null as the result');
    expect(auth.authState, AuthState.UNAUTHENTICATED,
        reason: 'User is not in an UNAUTHENTICATED state after signing out!');
  });

  test('Fail to create an existing user with email address and password',
      () async {
    String uid;
    try {
      uid = await auth.createUserWithEmailAndPassword(
          'testemailaddress@testmail.com', '123456');
    } catch (e) {
      PlatformException pe = e as PlatformException;
      expect(pe, isNotNull,
          reason:
              'An exception was thrown, but it was not a PlatformException');
      expect(pe.code, 'ERROR_EMAIL_ALREADY_IN_USE',
          reason:
              'An exception was thrown, but it was not the expected PlatformException code: ERROR_EMAIL_ALREADY_IN_USE');
    }
    expect(uid, isNull,
        reason: 'signInWithEmailAndPassword did not return null as the result');

    expect(auth.authState, AuthState.UNAUTHENTICATED,
        reason: 'User is not in an UNAUTHENTICATED state after signing out!');
  });

  test('Create user with email address and password', () async {
    var uid = await auth.createUserWithEmailAndPassword(
        'doesnotexistyet@testmail.com', '123456');
    expect(uid, 'test_uid',
        reason:
            'signInWithEmailAndPassword did not return "test_uid" as the result');
    await auth.signOut();
    expect(auth.authState, AuthState.UNAUTHENTICATED,
        reason: 'User is not in an UNAUTHENTICATED state after signing out!');
  });

  test('Send password reset email', () async {
    await auth.sendPasswordResetEmail('testemailaddress@testmail.com');
  });

  test('Trigger unhandled MockFacebookLogin channel message', () async {
    MockFacebookLoginChannel.throwOnUnhandledChannelMessages = true;
    var channel = MethodChannel(MockFacebookLoginChannel.platformChannelName);
    try {
      await channel.invokeMethod('thisMethodDoesNotExist');
    } on PlatformException catch (e) {
      expect(e.code, 'UnhandledMockChannelCall',
          reason:
              'An exception was thrown but was not UnhandledMockChannelCall');
    }
    MockFacebookLoginChannel.throwOnUnhandledChannelMessages = false;
  });

  test('Trigger unhandled MockFirebaseAuthChannel channel message', () async {
    MockFirebaseAuthChannel.throwOnUnhandledChannelMessages = true;
    var channel = MethodChannel(MockFirebaseAuthChannel.platformChannelName);
    try {
      await channel.invokeMethod('thisMethodDoesNotExist');
    } on PlatformException catch (e) {
      expect(e.code, 'UnhandledMockChannelCall',
          reason:
              'An exception was thrown but was not UnhandledMockChannelCall');
    }
    MockFirebaseAuthChannel.throwOnUnhandledChannelMessages = false;
  });

  test('Trigger unhandled MockFirebaseAuthChannel channel message', () async {
    MockGoogleSignInChannel.throwOnUnhandledChannelMessages = true;
    var channel = MethodChannel(MockGoogleSignInChannel.platformChannelName);
    try {
      await channel.invokeMethod('thisMethodDoesNotExist');
    } on PlatformException catch (e) {
      expect(e.code, 'UnhandledMockChannelCall',
          reason:
              'An exception was thrown but was not UnhandledMockChannelCall');
    }
    MockGoogleSignInChannel.throwOnUnhandledChannelMessages = false;
  });

  test(
      'Trigger a user not found error when sending null data to MockFirebaseAuth',
      () async {
    var channel = MethodChannel(MockFirebaseAuthChannel.platformChannelName);
    var callArguments = Map<String, dynamic>();
    callArguments['provider'] = 'password';
    callArguments['data'] = null;
    try {
      await channel.invokeMethod('signInWithCredential', callArguments);
    } on PlatformException catch (e) {
      expect(e.code, 'ERROR_USER_NOT_FOUND',
          reason: 'An exception was thrown but was not ERROR_USER_NOT_FOUND');
    }
  });

  test('Always Throw Up for Facebook', () async {
    MockFacebookLoginChannel.throwOnEveryChannelMessage =
        PlatformException(code: 'ALWAYS_THROW_UP');
    var exceptionCaught = false;
    try {
      await auth.signInWithFacebook();
    } on PlatformException catch (e) {
      MockFacebookLoginChannel.throwOnEveryChannelMessage = null;
      expect(e.code, 'ALWAYS_THROW_UP',
          reason:
              'Expected the Platform exception code to send ALWAYS_THROW_UP');
      exceptionCaught = true;
    } finally {
      MockFacebookLoginChannel.throwOnEveryChannelMessage = null;
    }
    expect(exceptionCaught, true,
        reason: 'An exception should have been thrown, but was not');
    MockFacebookLoginChannel.throwOnEveryChannelMessage = null;
  });

  test('Always Throw Up for Google', () async {
    MockGoogleSignInChannel.throwOnEveryChannelMessage =
        PlatformException(code: 'ALWAYS_THROW_UP');
    var exceptionCaught = false;
    try {
      await auth.signInWithGoogle();
    } on PlatformException catch (e) {
      MockGoogleSignInChannel.throwOnEveryChannelMessage = null;
      expect(e.code, 'ALWAYS_THROW_UP',
          reason:
              'Expected the Platform exception code to send ALWAYS_THROW_UP');
      exceptionCaught = true;
    } finally {
      MockGoogleSignInChannel.throwOnEveryChannelMessage = null;
    }
    expect(exceptionCaught, true,
        reason: 'An exception should have been thrown, but was not');
    MockGoogleSignInChannel.throwOnEveryChannelMessage = null;
  });

  test('Always Throw Up for Firebase', () async {
    MockFirebaseAuthChannel.throwOnEveryChannelMessage =
        PlatformException(code: 'ALWAYS_THROW_UP');
    var exceptionCaught = false;
    try {
      await auth.signInWithEmailAndPassword('foo', 'bar');
    } on PlatformException catch (e) {
      MockFirebaseAuthChannel.throwOnEveryChannelMessage = null;
      expect(e.code, 'ALWAYS_THROW_UP',
          reason:
              'Expected the Platform exception code to send ALWAYS_THROW_UP');
      exceptionCaught = true;
    } finally {
      MockFirebaseAuthChannel.throwOnEveryChannelMessage = null;
    }
    expect(exceptionCaught, true,
        reason: 'An exception should have been thrown, but was not');
    MockFirebaseAuthChannel.throwOnEveryChannelMessage = null;
  });
}
