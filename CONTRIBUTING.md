# Contributing

Contributions are welcome if the contribution is legal and qualified.
To make a contribution, be sure to create a new issue to track it.

## (TLDR;) Too Long, Didn't Read

Don't steal code and post it here, don't post bad code and don't be a
jerk.

## Quality

Perfection is a direction, not a destination. Better is better, but
not always perfect for everyone. A change has to be generally better.

### Testing and readability

A pull request (PR) should include tests, and not decrease test
coverage for the package. It should be readable, easy to use, have
reasonable defaults so anyone can grab the package and use it without
much fuss.

### Documentation

It should include any /// documentation for exported or publicly
accessible member data or methods. New functionality is not helpful
if it is silent. The triple slash documentation is ubiquitous in most
IDEs and is very helpful. Include some examples if possible!

The CHANGELOG.md file should be updated if applicable.

The README.md should be updated if people landing on the project
page would care about your change. If it is an internal bug fix, that
is probably not worthy of publication. If it is introducing some new
capability that programmers are interested in, it may be. Use good
judgment. If the PR fixes a bug that always crashes a new device, that
is probably worthy of a CHANGELOG.md and README.md update and singing
praises from the rooftops if there are a ton of people affected or
interested.

## Decorum

[Be an adult](http://www.wheatonslaw.com/), collegiate, and remember,
this is about the code, not the coder. Extra credit for also being
helpful to others.

## Code review process

Reveiw the code, not the coder. Refer to the rule above,
"[Be an adult](http://www.wheatonslaw.com/)". Code is reviewed for
correctness, readability, maintainability, and quality.

## Unencumbered contribution

You may not contribute code that someone else has any claim to own,
patent or copyright. Nobody needs that kind of legal headache, so do
not submit any changes that would violate any contracts, licenses,
patents, copyright or other legal claims. If you submit a pull request,
you have this policy and are acting on authority to make submissions.

Grabbing passports and verifications are silly. You have the license
and the contribution guidelines and are bound by these not too
difficult rules to follow: you are not legally encumbered, your change
is good, you are an adult that is capable of adhereing to these
guidelines.
