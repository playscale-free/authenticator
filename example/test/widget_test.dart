// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:authenticator/authenticator.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../lib/main.dart';

void main() {
  testWidgets('Can sign in and sign out', (WidgetTester tester) async {
    var auth = Authenticator.createMocked();
    // Build our app and trigger a frame.
    await tester.pumpWidget(MyApp(auth));

    // Should not be in an AUTHENTICATED state, and the Login page
    // should be showing.
    expect(auth.authState, isNot(AuthState.AUTHENTICATED),
        reason: 'Authentication state should not yet be AUTHENTICATED');
    expect(find.byKey(ValueKey('Login Page')), findsOneWidget,
        reason: 'The login page was not showing when it should have been');
    expect(find.byKey(ValueKey('signInWithGoogle button')), findsOneWidget,
        reason:
            'The Google sign in button was not showing when it should have been');

    // Perform a mocked login using Google
    await tester.tap(find.byKey(ValueKey('signInWithGoogle button')));
    await tester.pumpAndSettle();

    // Depending on the auth provider, it may be in an
    // AUTHENTICATING state between frames.
    while (auth.authState == AuthState.AUTHENTICATING) {
      await tester.pumpAndSettle();
    }

    // Should be in an AUTHENTICATED state, and the home page should
    // be showing with a sign out button available.
    expect(auth.authState, AuthState.AUTHENTICATED,
        reason: 'The user should have been authenticated, but was not');
    expect(find.byKey(ValueKey('Home Page')), findsOneWidget,
        reason: 'The Home Page should have been showing, but was not');
    expect(find.byKey(ValueKey('signOut button')), findsOneWidget,
        reason: 'The sign out button was not visible on the home page');

    // Sign out should rebuild the widget tree with the login page
    // showing again and the user no longer AUTHENTICATED.
    await tester.tap(find.byKey(ValueKey('signOut button')));
    await tester.pumpAndSettle();
    expect(auth.authState, isNot(AuthState.AUTHENTICATED),
        reason: 'Authentication state should not yet be AUTHENTICATED');
    expect(find.byKey(ValueKey('Login Page')), findsOneWidget,
        reason: 'The login page was not showing when it should have been');
    expect(find.byKey(ValueKey('signInWithFacebook button')), findsOneWidget,
        reason:
            'The Facebook sign in button was not showing when it should have been');

    // Perform a mocked login using Facebook
    await tester.tap(find.byKey(ValueKey('signInWithFacebook button')));
    await tester.pumpAndSettle();

    // Depending on the auth provider, it may be in an
    // AUTHENTICATING state between frames.
    while (auth.authState == AuthState.AUTHENTICATING) {
      await tester.pumpAndSettle();
    }
    // Should be in an AUTHENTICATED state, and the home page should
    // be showing with a sign out button available.
    expect(auth.authState, AuthState.AUTHENTICATED,
        reason: 'The user should have been authenticated, but was not');
    expect(find.byKey(ValueKey('Home Page')), findsOneWidget,
        reason: 'The Home Page should have been showing, but was not');
    expect(find.byKey(ValueKey('signOut button')), findsOneWidget,
        reason: 'The sign out button was not visible on the home page');
  });
}
