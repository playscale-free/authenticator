// Copyright (c) 2019 Playscale PTE LTD - All Rights Reserved
// See LICENSE, included with this distribution, for more details
// email: contact@playscale.io

import 'package:flutter/services.dart';
import 'dart:convert';

// Note, commented out code here are calls that could be made to
// the platform channel, but are currently not triggered by
// Authenticator. If a Missing method or plugin exception is thrown
// after adding a feature to Authinticator, have a look here too
// see if the method is commented out. It will still fail unit tests,
// but is a starting point to mock the platform channel for tests.

enum _MockFirebaseAuthChannelAuthState {
  UNINITIALIZED,
  UNAUTHENTICATED,
  AUTHENTICATING,
  AUTHENTICATED
}

/// Replaces platform channel responses to requests from
/// 'package:firebase_auth/firebase_auth.dart'.
///
/// Instantiate this by calling
/// ```dart
/// MockFirebaseAuthChannel();
/// ```
/// There are static members that can be set prior to
/// authentication that will affect the behavior of this mock.
/// You can override the responses by providing your own json
/// data (logIn and getCurrentToken), as well as enabling
/// extra debugging and assertions by setting
/// ```dart
/// throwOnUnhandledChannelMessages = true|false
/// ```
/// See also static String
/// ```dart
/// MockFirebaseAuthChannel.authResultResponse
/// ```
/// ```dart
/// MockFirebaseAuthChannel.idTokenResponse
/// ```
/// ```dart
/// MockFirebaseAuthChannel.mockCurrentSignInProvider
/// ```
/// ```dart
/// MockFirebaseAuthChannel.mockPassword
/// ```
/// documentation for more information. In VSCode, just hover
/// over any declaration of either to get more information.
class MockFirebaseAuthChannel {
  /// Replace MockFirebaseAuthChannel.authResultResponse with
  /// custom values.
  ///
  /// Timestamps, if set to 0, will
  /// be calculated based on the current time, or in the case of an
  /// expiry, the current time plus 24 hours.
  ///
  /// The defaults are (from a json perspective):
  /// ```json
  /// {
  ///     "additionalUserInfo": {
  ///         "providerId": "google.com",
  ///         "profile": {
  ///             "given_name": "Test",
  ///             "locale": "en",
  ///             "family_name": "User",
  ///             "picture": "https://picsum.photos/id/418/400/400",
  ///             "hd": "playscale.io",
  ///             "aud": "testmail.com",
  ///             "azp": "testmail.com",
  ///             "exp": 0, // this will be calculated as ((DateTime.now().millisecondsSinceEpoch / 1000) + (60 * 60 * 24)).round()
  ///             "iat": 0, // this will be calculated as (DateTime.now().millisecondsSinceEpoch / 1000).round()
  ///             "iss": "https://accounts.google.com",
  ///             "sub": "42",
  ///             "name": "Test User",
  ///             "email": "testemailaddress@testmail.com",
  ///             "email_verified": true
  ///         },
  ///         "isNewUser": false,
  ///         "username": null
  ///     },
  ///     "user": {
  ///         "uid": "test_uid",
  ///         "photoUrl": "https://picsum.photos/id/418/400/400",
  ///         "isAnonymous": false,
  ///         "providerData": [{
  ///             "uid": "test_uid",
  ///             "photoUrl": "https://picsum.photos/id/418/400/400",
  ///             "providerId": "firebase",
  ///             "displayName": "Test User",
  ///             "email": "testemailaddress@testmail.com"
  ///         },
  ///         {
  ///             "uid": "42",
  ///             "photoUrl": "https://picsum.photos/id/418/400/400",
  ///             "providerId": "google.com",
  ///             "displayName": "Test User",
  ///             "email": "testemailaddress@testmail.com"
  ///         }],
  ///         "providerId": "firebase",
  ///         "displayName": "Test User",
  ///         "creationTimestamp": 0, // this will be calculated as (DateTime.now().millisecondsSinceEpoch / 1000).round()
  ///         "lastSignInTimestamp": 0, // this will be calculated as (DateTime.now().millisecondsSinceEpoch / 1000).round()
  ///         "email": "testemailaddress@testmail.com",
  ///         "isEmailVerified": true
  ///     }
  /// }
  /// ```
  static Map authResultResponse = json.decode(_authResultResponse);

  /// Resets authResultResponse with default values.
  static void resetAuthResultResponse() {
    if (authResultResponse != null) {
      authResultResponse.clear();
    }
    authResultResponse = json.decode(_authResultResponse);
  }

  static String _authResultResponse = '''
{
    "additionalUserInfo": {
        "providerId": "google.com", 
        "profile": {
            "given_name": "Test", 
            "locale": "en", 
            "family_name": "User", 
            "picture": "https://picsum.photos/id/418/400/400", 
            "hd": "playscale.io",
            "aud": "testmail.com", 
            "azp": "testmail.com", 
            "exp": 0, 
            "iat": 0, 
            "iss": "https://accounts.google.com", 
            "sub": "42", 
            "name": "Test User", 
            "email": "testemailaddress@testmail.com", 
            "email_verified": true
        }, 
        "isNewUser": false, 
        "username": null
    },
    "user": {
        "uid": "test_uid",
        "photoUrl": "https://picsum.photos/id/418/400/400",
        "isAnonymous": false,
        "providerData": [{
            "uid": "test_uid",
            "photoUrl": "https://picsum.photos/id/418/400/400",
            "providerId": "firebase",
            "displayName": "Test User",
            "email": "testemailaddress@testmail.com"
        }, 
        {
            "uid": "42", 
            "photoUrl": "https://picsum.photos/id/418/400/400",
            "providerId": "google.com",
            "displayName": "Test User",
            "email": "testemailaddress@testmail.com"
        }], 
        "providerId": "firebase", 
        "displayName": "Test User", 
        "creationTimestamp": 0, 
        "lastSignInTimestamp": 0, 
        "email": "testemailaddress@testmail.com", 
        "isEmailVerified": true
    }
}
  ''';

  /// Replace MockFirebaseAuthChannel.idTokenResponse with custom
  /// values to alter the behavior of the mocked channel. The
  /// defaults should be reasonable for most cases.
  ///
  /// Timestamps, if set to 0, will
  /// be calculated based on the current time, or in the case of an
  /// expiry, the current time plus 24 hours.
  ///
  /// The default values are:
  /// ```json
  /// {
  ///     "issuedAtTimestamp": 0, // this will be calculated as (DateTime.now().millisecondsSinceEpoch / 1000).round()
  ///     "authTimestamp": 0, // this will be calculated as (DateTime.now().millisecondsSinceEpoch / 1000).round()
  ///     "signInProvider": "google.com",
  ///     "expirationTimestamp": 0, // this will be calculated as ((DateTime.now().millisecondsSinceEpoch / 1000) + (60 * 60 * 24)).round()
  ///     "token": "test_token",
  ///     "claims": {
  ///         "picture": "https://picsum.photos/id/418/400/400",
  ///         "firebase": {
  ///             "identities": {
  ///                 "google.com": [
  ///                     42
  ///                 ],
  ///                 "email": [
  ///                     "testemailaddress@testmail.com"
  ///                 ]
  ///             },
  ///             "sign_in_provider": "google.com"
  ///         },
  ///         "user_id": "test_uid",
  ///         "aud": "test_app",
  ///         "exp": 0, // this will be calculated as ((DateTime.now().millisecondsSinceEpoch / 1000) + (60 * 60 * 24)).round()
  ///         "iat": 0, // this will be calculated as (DateTime.now().millisecondsSinceEpoch / 1000).round()
  ///         "iss": "https://testmail.com",
  ///         "sub": "test_subject",
  ///         "name": "Test User",
  ///         "email": "testemailaddress@testmail.com",
  ///         "email_verified": true,
  ///         "auth_time": 0
  ///     }
  /// }
  /// ```
  static Map idTokenResponse = json.decode(_idTokenResponse);

  /// Resets idTokenResponse values to defaults.
  static void resetIdTokenResponse() {
    if (idTokenResponse != null) {
      idTokenResponse.clear();
    }
    idTokenResponse = json.decode(_idTokenResponse);
  }

  static String _idTokenResponse = '''
{
    "issuedAtTimestamp": 0,
    "authTimestamp": 0,
    "signInProvider": "google.com",
    "expirationTimestamp": 0,
    "token": "test_token",
    "claims": {
        "picture": "https://picsum.photos/id/418/400/400", 
        "firebase": {
            "identities": {
                "google.com": [
                    42
                ], 
                "email": [
                    "testemailaddress@testmail.com"
                ]
            }, 
            "sign_in_provider": "google.com"
        }, 
        "user_id": "test_uid", 
        "aud": "test_app", 
        "exp": 0, 
        "iat": 0, 
        "iss": "https://testmail.com", 
        "sub": "test_subject", 
        "name": "Test User", 
        "email": "testemailaddress@testmail.com", 
        "email_verified": true, 
        "auth_time": 0
    }
}
  ''';

  /// Override this if your tests require a different provider
  /// in the AuthResult
  static String mockCurrentSignInProvider = 'firebase';

  /// Pretend this is the only user in the database, useful
  /// for testing failure cases where either the user is not found
  /// or the user exists while creating a new account.
  static String mockEmail = "testemailaddress@testmail.com";

  /// Pretend that the only user's password is set to this valus.
  /// Convenient for testing bad password failure cases.
  static String mockPassword = '123456';

  /// When set to
  /// `true`
  /// it will trigger an assert in debug builds that crash tests.
  /// If this happens, it means that a call that was made to the
  /// mocked platform channel was unhandled and needs to be implemented
  /// in. Defaults to ```false```.
  static bool throwOnUnhandledChannelMessages = false;

  /// When set to an exception, this will throw the specified
  /// exception. This is useful for testing
  /// boilerplate try/catch code in tests for coverage and correctness.
  ///
  /// Defaults to `null`
  static Exception throwOnEveryChannelMessage;

  static const String platformChannelName = 'plugins.flutter.io/firebase_auth';
  static final _firebaseAuthChannel = MethodChannel(platformChannelName);
  static _MockFirebaseAuthChannelAuthState _authState =
      _MockFirebaseAuthChannelAuthState.UNINITIALIZED;

  static final _loginBadPasswordErrorCode = 'ERROR_WRONG_PASSWORD';
  static final _loginUserNotFoundErrorCode = 'ERROR_USER_NOT_FOUND';
  static final _registrationEmailInUseErrorCode = 'ERROR_EMAIL_ALREADY_IN_USE';

  MockFirebaseAuthChannel() {
    _firebaseAuthChannel.setMockMethodCallHandler(_methodCall);
  }

  Future<dynamic> _methodCall(MethodCall call) async {
    // print(call);
    if (throwOnEveryChannelMessage != null) {
      throw (throwOnEveryChannelMessage);
    }
    if (authResultResponse != null) {
      // calculate issued at and expiration dates in the result data if needed
      if (authResultResponse.containsKey('additionalUserInfo') &&
          authResultResponse['additionalUserInfo'].containsKey('profile')) {
        if (authResultResponse['additionalUserInfo']['profile']
                .containsKey('exp') &&
            authResultResponse['additionalUserInfo']['profile']['exp'] == 0) {
          authResultResponse['additionalUserInfo']['profile']['exp'] =
              ((DateTime.now().millisecondsSinceEpoch / 1000) + (60 * 60 * 24))
                  .round();
        }
        if (authResultResponse['additionalUserInfo']['profile']
                .containsKey('iat') &&
            authResultResponse['additionalUserInfo']['profile']['iat'] == 0) {
          authResultResponse['additionalUserInfo']['profile']['iat'] =
              (DateTime.now().millisecondsSinceEpoch / 1000).round();
        }
      }
      if (authResultResponse.containsKey('user') &&
          authResultResponse['user'].containsKey('creationTimestamp') &&
          authResultResponse['user']['creationTimestamp'] == 0) {
        authResultResponse['user']['creationTimestamp'] =
            (DateTime.now().millisecondsSinceEpoch / 1000).round();
      }
      if (authResultResponse.containsKey('user') &&
          authResultResponse['user'].containsKey('lastSignInTimestamp') &&
          authResultResponse['user']['lastSignInTimestamp'] == 0) {
        authResultResponse['user']['lastSignInTimestamp'] =
            (DateTime.now().millisecondsSinceEpoch / 1000).round();
      }
    }
    if (idTokenResponse != null) {
      if (idTokenResponse['issuedAtTimestamp'] == 0) {
        idTokenResponse['issuedAtTimestamp'] =
            (DateTime.now().millisecondsSinceEpoch / 1000).round();
      }
      if (idTokenResponse['authTimestamp'] == 0) {
        idTokenResponse['authTimestamp'] =
            (DateTime.now().millisecondsSinceEpoch / 1000).round();
      }
      if (idTokenResponse['claims']['exp'] == 0) {
        ((DateTime.now().millisecondsSinceEpoch / 1000) + (60 * 60 * 24))
            .round();
      }
      if (idTokenResponse['claims']['iat'] == 0) {
        idTokenResponse['claims']['iat'] =
            (DateTime.now().millisecondsSinceEpoch / 1000).round();
      }
      if (idTokenResponse['claims']['auth_time'] == 0) {
        idTokenResponse['claims']['auth_time'] =
            (DateTime.now().millisecondsSinceEpoch / 1000).round();
      }
    }
    switch (call.method) {
      case 'signInWithCredential':
        _authState = _MockFirebaseAuthChannelAuthState.AUTHENTICATING;
        if (call.arguments['provider'] == 'password') {
          var data = call.arguments['data'];
          if (data != null) {
            // search for deliberate mismatch
            if (data['email'] != authResultResponse['user']['email']) {
              // No such user
              _authState = _MockFirebaseAuthChannelAuthState.UNAUTHENTICATED;
              throw (PlatformException(code: _loginUserNotFoundErrorCode));
            }
            if (data['password'] != mockPassword) {
              // bad password
              _authState = _MockFirebaseAuthChannelAuthState.UNAUTHENTICATED;
              throw (PlatformException(code: _loginBadPasswordErrorCode));
            }
          } else {
            _authState = _MockFirebaseAuthChannelAuthState.UNAUTHENTICATED;
            throw (PlatformException(code: _loginUserNotFoundErrorCode));
          }
        }

        _authState = _MockFirebaseAuthChannelAuthState.AUTHENTICATED;
        return authResultResponse;
        break;

      case 'currentUser': // From firebase_auth.dart
        if (_authState == _MockFirebaseAuthChannelAuthState.AUTHENTICATED) {
          return authResultResponse['user'];
        }
        return null;
        break;
      case 'sendPasswordResetEmail': // From firebase_auth.dart
        return null; // this is a mock up, no network, no email sent
        break;
      // firebase_auth.dart, returns List<String>
      // case 'fetchSignInMethodsForEmail':
      //   assert(false);
      //   return null;
      //   break;
      // case 'sendLinkToEmail': // From firebase_auth.dart
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // case 'isSignInWithEmailLink': // From firebase_auth.dart
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // case 'verifyPhoneNumber': // From firebase_auth.dart
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      case 'signOut': // From firebase_auth.dart
        _authState = _MockFirebaseAuthChannelAuthState.UNAUTHENTICATED;
        return null;
        break;
      // case 'setLanguageCode': // From firebase_auth.dart
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // case 'logOut':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      // case 'reauthenticateWithCredential':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      // case 'updateProfile':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      // case 'unlinkFromProvider':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      case 'getIdToken':
        return idTokenResponse;
        break;
      // From firebase_user.dart
      // case 'linkWithCredential':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      // case 'sendEmailVerification':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      // case 'reload':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      // case 'delete':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      // case 'updateEmail':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      // case 'updatePhoneNumberCredential':
      //   assert(false); // TODO find a test to trigger this and implement it
      //   return null;
      //   break;
      // From firebase_user.dart
      case 'createUserWithEmailAndPassword':
        if (call.arguments['email'] == mockEmail) {
          _authState = _MockFirebaseAuthChannelAuthState.UNAUTHENTICATED;
          throw (PlatformException(code: _registrationEmailInUseErrorCode));
        }
        _authState = _MockFirebaseAuthChannelAuthState.AUTHENTICATED;
        return authResultResponse;
        break;
      default:
        if (throwOnUnhandledChannelMessages) {
          throw (PlatformException(
              code: 'UnhandledMockChannelCall', message: "$call"));
        }
        break;
    }

    return null;
  }
}
