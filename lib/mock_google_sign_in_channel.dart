// Copyright (c) 2019 Playscale PTE LTD - All Rights Reserved
// See LICENSE, included with this distribution, for more details
// email: contact@playscale.io

import 'package:flutter/services.dart';
import 'dart:convert';

/// Replaces platform channel responses to requests from
/// 'package:google_sign_in/google_sign_in.dart'.
///
/// Instantiate this by calling
/// ```dart
/// MockGoogleSignInChannel();
/// ```
/// There are four static members that can be set prior to
/// authentication that will affect the behavior of this mock.
/// You can override the responses by providing your own json
/// data (logIn and getCurrentToken), as well as enabling
/// extra debugging and assertions by setting
/// ```dart
/// throwOnUnhandledChannelMessages = true|false
/// ```
/// See also static String
/// ```dart
/// MockGoogleSignInChannel.googleSignInResponse
/// ```
/// documentation for more information. In VSCode, just hover
/// over any declaration of either to get more information.
class MockGoogleSignInChannel {
  /// When set to
  /// `true`
  /// it will trigger an assert in debug builds that crash tests.
  /// If this happens, it means that a call that was made to the
  /// mocked platform channel was unhandled and needs to be implemented
  /// in. Defaults to ```false```.
  static bool throwOnUnhandledChannelMessages = false;

  /// When set to an exception, this will throw the specified
  /// exception. This is useful for testing
  /// boilerplate try/catch code in tests for coverage and correctness.
  ///
  /// Defaults to `null`
  static Exception throwOnEveryChannelMessage;

  /// Change this to modify the result return to google_sign_in.
  /// The default is:
  /// ```json
  /// {
  ///   "photoUrl": "https://picsum.photos/id/418/400/400",
  ///   "displayName": "Test User",
  ///   "idToken": "test_id_token",
  ///   "accessToken": "test_access_token",
  ///   "id": "42",
  ///   "email": "testemailaddress@testmail.com"
  /// }
  /// ```
  static Map googleSignInResponse = json.decode(_googleSignInResponse);

  /// Sets the googleSignInResponse back to defaults.
  /// See `googleSignInResponse` for information.
  static void resetGoogleSignInResponse() {
    if (googleSignInResponse != null) {
      googleSignInResponse.clear();
    }
    googleSignInResponse = json.decode(_googleSignInResponse);
  }

  static const String _googleSignInResponse = '''
{
  "photoUrl": "https://picsum.photos/id/418/400/400",
  "displayName": "Test User",
  "idToken": "test_id_token",
  "accessToken": "test_access_token",
  "id": "42",
  "email": "testemailaddress@testmail.com"
}  
  ''';

  static const String platformChannelName = 'plugins.flutter.io/google_sign_in';
  static final _googleSignInChannel = MethodChannel(platformChannelName);
  MockGoogleSignInChannel() {
    _googleSignInChannel.setMockMethodCallHandler(_methodCall);
  }
  Future<dynamic> _methodCall(MethodCall call) async {
    // print(call);
    if (throwOnEveryChannelMessage != null) {
      throw (throwOnEveryChannelMessage);
    }
    switch (call.method) {
      case 'init':
        return googleSignInResponse;
        break;
      case 'signIn':
        return googleSignInResponse;
        break;
      case 'getTokens':
        return googleSignInResponse;
        break;
      // case 'signInSilently':
      //   return googleSignInResponse;
      //   break;
      // Not yet called by Authenticator
      // case 'isSignedIn':
      //   return _parent.authState == AuthState.AUTHENTICATED;
      //   break;
      case 'signOut':
        return null;
        break;
      // Not yet called by Authenticator
      // case 'disconnect':
      //   return googleSignInResult;
      //   break;
      default:
        if (throwOnUnhandledChannelMessages) {
          throw (PlatformException(
              code: 'UnhandledMockChannelCall', message: "$call"));
        }
        break;
    }

    return null;
  }
}
